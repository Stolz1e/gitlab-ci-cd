import random
import string

def generate_passwd(min_length=8, max_length=32):
    if min_length > max_length:
        return "Минимальная длина не может быть больше максимальной"

    length = random.randint(min_length, max_length)
    characters = string.ascii_letters + string.digits + string.punctuation
    password = ''.join(random.choice(characters) for i in range(length))
    return password

# Генерация пароля
sample_passwd = generate_passwd()
print(f"Ваш пароль: {sample_passwd}")